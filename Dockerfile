FROM golang:1.16-alpine

WORKDIR /stellarsys

COPY stellarsys_src/ ./
RUN go mod download


RUN go build -o /stellarsys

CMD ["./stellarsys"]