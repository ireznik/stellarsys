package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"stellarsys/db/model"
	"stellarsys/device"

	"gorm.io/gorm"
)

type sessions map[string]*model.ActiveSession

type Stellarsys struct {
	sessionHandler model.SessionHandler
	devicePool     device.DevicePool
}

func CreateStellarsys(dbConnection *gorm.DB) *Stellarsys {
	stellarsys := Stellarsys{
		sessionHandler: model.NewSessionHandler(dbConnection),
		devicePool:     device.NewDevicePool(dbConnection),
	}

	return &stellarsys
}

func (s *Stellarsys) FindDevice(deviceType model.DeviceType) (*model.Device, error) {
	fd, err := s.devicePool.FindDeviceInPool(deviceType)

	if err != nil {
		return fd, err
	}

	return fd, nil
}

func (s *Stellarsys) AllocateDevice(d *model.Device, user string) (*model.ActiveSession, error) {
	as := model.GenerateSession(d.ID, user)

	// TODO: wrap in transaction
	_, err := s.sessionHandler.AddSession(as)
	if err != nil {
		return as, err
	}

	err = s.devicePool.AllocateDevice(*d, as.Session)

	if err != nil {
		return &model.ActiveSession{}, err
	}

	return as, nil
}

func (s *Stellarsys) FreeDevice(deviceSession string) error {
	as, err := s.sessionHandler.FindSession(deviceSession)
	if err != nil {
		return err
	}

	_, err = s.sessionHandler.RemoveSession(deviceSession)
	if err != nil {
		return err
	}

	_, err = s.devicePool.FreeDevice(as.Session)

	// TODO: wrap in transaction
	if *as == (model.ActiveSession{}) {
		return errors.New("the provided session is not valid")
	}

	if err != nil {
		return err
	}

	return nil
}

func (s *Stellarsys) HandleAllocateRequest(w http.ResponseWriter, r *http.Request) {
	response := Response{}
	w.Header().Set("Content-Type", JSON_CONTENT_TYPE)
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		sendErrorResponse(w, err, http.StatusInternalServerError)
		return
	}

	var requestBody model.AllocateDeviceData
	err = json.Unmarshal(body, &requestBody)
	if err != nil {
		sendErrorResponse(w, err, http.StatusInternalServerError)
		return
	}

	d, err := s.FindDevice(requestBody.Type)
	if err != nil {
		w.WriteHeader(http.StatusOK)
		response.Status = http.StatusOK
		response.Payload.Message = err.Error()
		json.NewEncoder(w).Encode(response)
		return
	}

	as, err := s.AllocateDevice(d, requestBody.User)
	if err != nil {
		sendErrorResponse(w, err, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	response.Status = http.StatusOK
	response.Payload.Session = as
	json.NewEncoder(w).Encode(response)
}

func (s *Stellarsys) HandleFreeRequest(w http.ResponseWriter, r *http.Request) {
	response := Response{}
	w.Header().Set("Content-Type", JSON_CONTENT_TYPE)
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if err != nil {
		sendErrorResponse(w, err, http.StatusInternalServerError)
		return
	}

	var requestBody model.FreeDeviceData
	err = json.Unmarshal(body, &requestBody)
	if err != nil {
		sendErrorResponse(w, err, http.StatusInternalServerError)
		return
	}

	err = s.FreeDevice(requestBody.Session)

	if err != nil {
		sendErrorResponse(w, err, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	response.Status = http.StatusOK
	response.Payload.Message = "device was freed"
	json.NewEncoder(w).Encode(response)
}

func sendErrorResponse(w http.ResponseWriter, err error, status int) {
	response := Response{}
	w.WriteHeader(http.StatusInternalServerError)
	response.Status = http.StatusInternalServerError
	response.Payload.Message = err.Error()

	json.NewEncoder(w).Encode(response)
}
