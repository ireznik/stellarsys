package device

import (
	"errors"
	"stellarsys/db/model"

	"gorm.io/gorm"
)

type DevicePool struct {
	db *gorm.DB
}

func NewDevicePool(db *gorm.DB) DevicePool {
	devicePool := DevicePool{db: db}

	return devicePool
}

func (dp DevicePool) FindDeviceInPool(deviceType model.DeviceType) (*model.Device, error) {
	foundDevice := model.Device{}
	result := dp.db.Where("type =? AND allocated =?", deviceType, false).Limit(1).Order("id").First(&foundDevice)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return &foundDevice, errors.New("no device could be found for the given criteria")
	}

	if result.Error != nil {
		return &foundDevice, nil
	}

	return &foundDevice, nil
}

func (dp *DevicePool) AllocateDevice(d model.Device, deviceSession string) error {
	result := dp.db.Model(&model.Device{}).Where("id = ?", d.ID).Updates(model.Device{Allocated: true, SessionId: deviceSession})
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (dp *DevicePool) FreeDevice(session string) (bool, error) {
	result := dp.db.Model(&model.Device{}).Where("session_id = ?", session).Updates(model.Device{Allocated: false})
	if result.Error != nil {
		return false, result.Error
	}

	return true, nil
}
