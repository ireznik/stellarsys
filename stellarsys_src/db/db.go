package db

import (
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var dsnTemplate = "host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Europe/Berlin"

func GetDatabaseConnection(host string, user string, password string, database string, port string, config *gorm.Config) (*gorm.DB, error) {
	dsn := fmt.Sprintf(dsnTemplate, host, user, password, database, port)
	db, err := gorm.Open(postgres.Open(dsn), config)

	if err != nil {
		return nil, err
	}

	return db, nil
}
