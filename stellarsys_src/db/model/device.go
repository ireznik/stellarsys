package model

import (
	"gorm.io/gorm"
)

type DeviceType string
type DeviceStatus string

const (
	ROVER     DeviceType = "ROVER"
	SATELLITE DeviceType = "SATELLITE"
)

const (
	NO_WARNING  DeviceStatus = "NO_WARNING"
	WARNING     DeviceStatus = "WARNING"
	MALFUNCTION DeviceStatus = "MALFUNCTION"
	OFFLINE     DeviceStatus = "OFFLINE"
	ONLINE      DeviceStatus = "ONLINE"
)

type Device struct {
	gorm.Model
	Type      DeviceType     `sql:"type:ENUM('ROVER','SATELLITE')" gorm:"not null"`
	Energy    int            `gorm:"not null"`
	Status    DeviceStatus   `gorm:"not null"`
	Allocated bool           `gorm:"not null"`
	SessionId string         `json:"-"`
	Session   *ActiveSession `gorm:"foreignkey:session;references:session_id"`
}

type AllocatedDevice struct {
	Id      int
	Session string
}

type AllocateDeviceData struct {
	Type   DeviceType   `json:"type"`
	Energy int          `json:"energy"`
	Status DeviceStatus `json:"status"`
	User   string       `json:"user,omitempty"`
}

type FreeDeviceData struct {
	Session string `json:"session,omitempty"`
}
