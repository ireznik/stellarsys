package model

import (
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"strconv"
	"time"

	"gorm.io/gorm"
)

type SessionHandler struct {
	db       *gorm.DB
	sessions map[string]ActiveSession
}

func NewSessionHandler(dbConnection *gorm.DB) SessionHandler {
	return SessionHandler{db: dbConnection, sessions: make(map[string]ActiveSession)}
}

type Database *gorm.DB

type ActiveSession struct {
	Session  string  `json:"session,omitempty" gorm:"primaryKey"`
	User     string  `json:"user,omitempty" gorm:"not null"`
	DeviceId int     `json:"device" gorm:"not null"`
	Device   *Device `json:"-" gorm:"foreignkey:id;references:device_id"`
}

func GenerateSession(deviceId uint, user string) *ActiveSession {
	sha1 := sha1.New()
	sha1.Write([]byte(strconv.FormatInt(time.Now().UnixNano(), 10)))
	sha1.Write([]byte(strconv.FormatInt(int64(deviceId), 10)))
	sha1.Write([]byte(user))

	return &ActiveSession{Session: hex.EncodeToString(sha1.Sum(nil)), User: user, DeviceId: int(deviceId)}
}

func (sh *SessionHandler) AddSession(as *ActiveSession) (bool, error) {
	if result := sh.db.Create(as); result.Error != nil {
		return false, result.Error
	}

	return true, nil
}

func (sh *SessionHandler) RemoveSession(session string) (bool, error) {
	if result := sh.db.Where("session = ?", session).Delete(&ActiveSession{}); result.Error != nil {
		return false, result.Error
	}

	return true, nil
}

func (sh *SessionHandler) FindSession(session string) (*ActiveSession, error) {
	as := ActiveSession{}
	result := sh.db.Where("session = ?", session).First(&as)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return &as, errors.New("invalid session id provided")
	}

	if result.Error != nil {
		return &as, result.Error
	}

	return &as, nil
}
