package db

import (
	"fmt"
	"stellarsys/db/model"

	"gorm.io/gorm"
)

func InitializeDatabase(db *gorm.DB) error {
	err := db.AutoMigrate(&model.ActiveSession{}, &model.Device{})
	if err != nil {
		fmt.Println(err.Error())
		return err

	}
	addDevices(db)
	return nil
}

func addDevices(db *gorm.DB) {
	db.Create(&model.Device{Type: model.SATELLITE, Energy: 100, Status: model.NO_WARNING, Allocated: false, SessionId: ""})
	db.Create(&model.Device{Type: model.ROVER, Energy: 98, Status: model.WARNING, Allocated: false, SessionId: ""})
	db.Create(&model.Device{Type: model.SATELLITE, Energy: 100, Status: model.ONLINE, Allocated: false, SessionId: ""})
}
