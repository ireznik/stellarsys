package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"stellarsys/db"
	"stellarsys/db/model"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gorm.io/gorm"
)

type Payload struct {
	Message string               `json:"message,omitempty"`
	Session *model.ActiveSession `json:"session,omitempty"`
}

type Response struct {
	Status  int     `json:"status"`
	Payload Payload `json:"payload"`
}

var ALLOCATION_URL = "/device/allocate"
var FREE_URL = "/device/free"
var JSON_CONTENT_TYPE = "application/json"

var dbHost string
var dbUser string
var dbPassword string
var dbName string
var dbPort string

var stellarsysPort string = ":8798"
var stellarsys *Stellarsys
var router = mux.NewRouter()

func main() {
	var env map[string]string
	env, err := godotenv.Read()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	setDatabaseVariables(env)

	initilaizationFlag := flag.Bool("initDb", true, "use flag to initialize database")
	flag.Parse()

	if *initilaizationFlag {
		initializeDatabase()
	}

	dbConnection, err := db.GetDatabaseConnection(dbHost, dbUser, dbPassword, dbName, dbPort, &gorm.Config{})
	if err != nil {
		log.Panic(err.Error())
		return
	}

	stellarsys = CreateStellarsys(dbConnection)
	initializeRouter()
}

func initializeDatabase() {
	dbConfigForMigration := &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
	}
	dbConnection, err := db.GetDatabaseConnection(dbHost, dbUser, dbPassword, dbName, dbPort, dbConfigForMigration)
	if err != nil {
		log.Panic(err.Error())
		return
	}

	err = db.InitializeDatabase(dbConnection)
	if err != nil {
		log.Panic(err)
		return
	}
}

func setDatabaseVariables(env map[string]string) {
	dbHost = env["DB_HOST"]
	dbUser = env["DB_USER"]
	dbPassword = env["DB_PASSWORD"]
	dbName = env["DB_NAME"]
	dbPort = env["DB_PORT"]
}

func initializeRouter() {
	router.HandleFunc(ALLOCATION_URL, stellarsys.HandleAllocateRequest).Methods(http.MethodPost)
	router.HandleFunc(FREE_URL, stellarsys.HandleFreeRequest).Methods(http.MethodDelete)

	fmt.Println()
	fmt.Printf("Start server on port %s", stellarsysPort)
	http.Handle("/", router)
	log.Fatal(http.ListenAndServe(stellarsysPort, router))
}
