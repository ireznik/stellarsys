package main

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"stellarsys/db"
	"testing"

	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
)

var validAllocationRequestBody = []byte(`{"type": "ROVER", "user": "asdf"}`)
var validAllocationRequestBodyWithoutAllocatableDevice = []byte(`{"type": "ISS", "user": "asdf"}`)
var invalidAllocationRequestBody = []byte(`{"type": "", "user": "asdf"}`)
var freeDeviceRequestBodyTemplate = `{"session": "%s"}`

var stellarsysInstance *Stellarsys

func init() {
	var env map[string]string
	env, err := godotenv.Read()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	dbHost = env["DB_HOST"]
	dbUser = env["DB_USER"]
	dbPassword = env["DB_PASSWORD"]
	dbName = env["DB_NAME"]
	dbPort = env["DB_PORT"]

	dbConnection, err := db.GetDatabaseConnection(dbHost, dbUser, dbPassword, dbName, dbPort, &gorm.Config{})
	if err != nil {
		panic(err)
	}

	stellarsysInstance = CreateStellarsys(dbConnection)
}

func TestSuccessfulDeviceAllocationForOneUser(t *testing.T) {
	assert := assert.New(t)

	req, err := http.NewRequest(http.MethodPost, ALLOCATION_URL, bytes.NewBuffer(validAllocationRequestBody))
	assert.Nil(err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(stellarsysInstance.HandleAllocateRequest)
	handler.ServeHTTP(rr, req)

	response, _ := io.ReadAll(rr.Result().Body)
	var responseBody Response
	err = json.Unmarshal(response, &responseBody)

	assert.Equal(http.StatusOK, rr.Code)
	assert.Nil(err)
	assert.Empty(responseBody.Payload.Message)
	assert.Equal(http.StatusOK, responseBody.Status)
	assert.NotEmpty(responseBody.Payload.Session.Session)
}

func TestSuccessfulRequestWithoutAllocation(t *testing.T) {
	assert := assert.New(t)

	req, err := http.NewRequest(http.MethodPost, ALLOCATION_URL, bytes.NewBuffer(validAllocationRequestBodyWithoutAllocatableDevice))
	assert.Nil(err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(stellarsysInstance.HandleAllocateRequest)
	handler.ServeHTTP(rr, req)

	response, _ := io.ReadAll(rr.Result().Body)
	var responseBody Response
	err = json.Unmarshal(response, &responseBody)

	assert.Equal(http.StatusOK, rr.Code)
	assert.Nil(err)
	assert.NotEmpty(responseBody.Payload.Message)
	assert.Empty(responseBody.Payload.Session)
}

func TestFailedRequestBecauseDeviceTypeIsMissing(t *testing.T) {
	assert := assert.New(t)

	req, err := http.NewRequest(http.MethodPost, ALLOCATION_URL, bytes.NewBuffer(invalidAllocationRequestBody))
	assert.Nil(err)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(stellarsysInstance.HandleAllocateRequest)
	handler.ServeHTTP(rr, req)

	response, _ := io.ReadAll(rr.Result().Body)
	var responseBody Response
	err = json.Unmarshal(response, &responseBody)

	assert.Equal(http.StatusOK, rr.Code)
	assert.Nil(err)
	assert.NotEmpty(responseBody.Payload.Message)
	assert.Equal("no device could be found for the given criteria", responseBody.Payload.Message)
	assert.Empty(responseBody.Payload.Session)
}
