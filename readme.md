## Start
cd stellarsys_src
go run . --initDb=false
or omit the option `--initDb` to run with a new database to create tables

## Test
cd stellarsys_src
go test .

## Build Dockerfile
`docker build --tag stellarsys .`
`docker run -p 8798:8798 -d stellarsys`