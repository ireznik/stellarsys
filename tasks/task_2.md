# Techstack

## Frontend
- React 
- Typescript
- Webpack / ViteJS / Rollup

## Backend
### Auth-Proxy
- Nginx + z.B. vouch-proxy (nginx Auth module zur Anbindung von Keycloak)
### Identity-Provider
- Keycloak
### Stellarsys Service / Server
- Java oder Go
### Datenbank
- Postgres -> zur Persistierung der Geräte
- optional: Sessionhandling -> Redis

### Device-Agent
- C, C++, Java oder GO
